import * as express from 'express';
import * as sequelize from 'sequelize';

const username = "madness";
const password = "madness";
const host = "be-pretty-server";
const port = 5432;
const dbName = "MADNESS";
const db = new sequelize(dbName, username, password, {
    dialect: 'postgres',
    host: host,
    port: port,
});

const PicksModel = db.define('picks', {
    name: sequelize.STRING,
    pick: sequelize.STRING,
    round: sequelize.INTEGER
}, {
    timestamps: false,
});

const WinnersModel = db.define('winners', {
    team: sequelize.STRING,
    round: sequelize.INTEGER
}, {
    timestamps: false,
});

const TeamsModel = db.define('teams', {
    name: {
        type: sequelize.STRING,
        primaryKey: true,
    },
    seed: sequelize.INTEGER
}, {
    timestamps: false,
});

const picks = db.models.picks;
const winners = db.models.winners;
const teams = db.models.teams;

const app = express();

app.use((crossRequest, result, next) => {
    result.header("Access-Control-Allow-Origin", "*");
    result.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-Width, Content-Type, Authorization, Accept',
    );
    if (crossRequest.method === 'OPTIONS') {
        result.sendStatus(200);
    } else {
        next();
    }
});

app.use('/teams', async (req, res) => {
    const allTeams = await teams.findAll();
    const response = allTeams.map(team => {
        return team.dataValues;
    });
    res.send(response);
});

app.use('/winners', async (req, res) => {
    const allWinners = await winners.findAll();
    const response = allWinners.map(winner => {
        return winner.dataValues;
    });
    res.send(response);
});

app.use('/picks', async (req, res) => {
    const name = req.query.name;
    const allPicks = await picks.findAll({ where: { name } });
    const response = allPicks.map(pick => {
        return pick.dataValues;
    });
    res.send(response);
});

app.listen(8080);
