import Kitura
import HeliumLogger
import SwiftyJSON
import SwiftKuery
import SwiftKueryPostgreSQL
import Foundation

let username = "madness"
let password = "madness"
let host = "be-pretty-server"
let port = Int32(5432)
let dbName = "MADNESS"
let connection = PostgreSQLConnection(host: host, port: port, options: [.databaseName(dbName), .userName(username), .password(password)])

//let connection = PostgreSQLConnection(url: URL(string: "Postgres://\(username):\(password)@\(host):\(port)/\(dbName)")!)

HeliumLogger.use()

let router = Router()

class Picks: Table {
    let tableName = "picks"
    let key = Column("id")
    let name = Column("name")
    let pick = Column("pick")
    let round = Column("round")
}

class Teams: Table {
    let tableName = "teams"
    let name = Column("name")
    let seed = Column("seed")
}

class Winners: Table {
    let tableName = "winners"
    let key = Column("id")
    let team = Column("team")
    let round = Column("round")
}

let picks = Picks()
let teams = Teams()
let winners = Winners()

func picksByName(name: String, round: Int?, callback: @escaping ([[String : Any?]]) -> Void) -> Void {
    connection.connect() { error in
        if let error = error {
            print("Error is \(error)")
            return
        }
        
        let query: Select
        if let round = round {
            query = Select(picks.name, picks.pick, picks.round, from: picks).where(picks.name == name && picks.round == round)
        } else {
            query = Select(picks.name, picks.pick, picks.round, from: picks).where(picks.name == name)
        }
        connection.execute(query: query) { result in
            if let rows = result.asRows {
                print(rows)
                callback(rows)
            } else if let queryError = result.asError {
                print("Something went wrong \(queryError)")
            }
        }
    }
}

func allTeams(_ callback: @escaping([[String: Any?]]) -> Void) -> Void {
    connection.connect() { error in
        if let error = error {
            print("Error is \(error)")
            return
        }
        
        let query = Select(teams.name, teams.seed, from: teams)
        connection.execute(query: query) { result in
            if let rows = result.asRows {
                callback(rows)
            } else if let queryError = result.asError {
                print("Something went wrong \(queryError)")
            }
        }
        
    }
}

func allWinners(_ callback: @escaping([[String: Any?]]) -> Void) -> Void {
    connection.connect() { error in
        if let error = error {
            print("Error is \(error)")
            return
        }
        
        let query = Select(winners.team, winners.round, from: winners)
        connection.execute(query: query) { result in
            if let rows = result.asRows {
                callback(rows)
            } else if let queryError = result.asError {
                print("Something went wrong \(queryError)")
            }
        }
        
    }
}

router.get("/picks") { request, response, next in
    response.headers.append("Access-Control-Allow-Origin", value: "*")
    guard let name = request.queryParameters["name"] else {
        try response.status(.badRequest).end()
        return
    }
    
    if let round = request.queryParameters["round"] {
        picksByName(name: name, round: Int(round), callback: { result in            
            response.send(json: result)
            return
        })
    } else {
        picksByName(name: name, round: nil , callback: { result in
            response.send(json: result)
            return
        })
    }
}

router.get("/teams") { request, response, next in
    response.headers.append("Access-Control-Allow-Origin", value: "*")
    allTeams() { result in
        response.send(json: result)
        return
    }
}

router.get("/winners") { request, response, next in
    response.headers.append("Access-Control-Allow-Origin", value: "*")
    allWinners() { result in
        response.send(json: result)
        return
    }
}

Kitura.addHTTPServer(onPort: 8080, with: router)
Kitura.run()

