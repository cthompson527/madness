import * as React from 'react';
import Table, { Team, Winner } from './Table';
import * as _ from 'lodash';
/* tslint:disable */
// import { Icon, Table } from 'semantic-ui-react';

class App extends React.Component {
  teams: { name: string, seed: number }[];
  cory: { pick: string, round: number }[];
  christina: { pick: string, round: number }[];
  matt: { pick: string, round: number }[];
  winners: { team: string, round: number }[];

  state = {
    coryScore: 0,
    mattScore: 0,
    christinaScore: 0,
    corySeedScore: 0,
    mattSeedScore: 0,
    christinaSeedScore: 0,
  }

  async componentWillMount() {
    // const teamsPromise = await fetch('http://192.168.1.150:8081/teams', { 
    const teamsPromise = await fetch('api/teams', { 
      method: 'get',
    });

    // const coryPromise = await fetch('http://192.168.1.150:8081/picks?name=Cory', { 
    const coryPromise = await fetch('api/picks?name=Cory', { 
      method: 'get',
    });

    // const christinaPromise = await fetch('http://192.168.1.150:8081/picks?name=Christina', { 
    const christinaPromise = await fetch('api/picks?name=Christina', { 
      method: 'get',
    });

    // const mattPromise = await fetch('http://192.168.1.150:8081/picks?name=Matt', { 
    const mattPromise = await fetch('api/picks?name=Matt', { 
      method: 'get',
    });

    // const winnersPromise = await fetch('http://192.168.1.150:8081/winners', { 
    const winnersPromise = await fetch('api/winners', { 
      method: 'get',
    });

    this.teams = await teamsPromise.json();
    this.cory = await coryPromise.json();
    this.christina = await christinaPromise.json();
    this.matt = await mattPromise.json();
    this.winners = await winnersPromise.json();

    let cScore = 0;
    let mScore = 0;
    let chScore = 0;
    let cSeedScore = 0;
    let mSeedScore = 0;
    let chSeedScore = 0;

    const correctPick = (name: string, winner: Winner) => {
      const n = name.toLowerCase();
      return _.find(this[n], { pick: winner.team, round: winner.round }) ? true : false;
    };

    const getSeed = (team: string) => {
      return (_.find(this.teams, { name: team }) as Team).seed;
    };

    this.winners.forEach(winner => {
      const seed = getSeed(winner.team);
      if (correctPick('Cory', winner)) {
        cScore += 2 ** (winner.round - 1);
        cSeedScore += 2 ** (winner.round - 1) * seed;
      }

      if (correctPick('Matt', winner)) {
        mScore += 2 ** (winner.round - 1);
        mSeedScore += 2 ** (winner.round - 1) * seed;
      }

      if (correctPick('Christina', winner)) {
        chScore += 2 ** (winner.round - 1);
        chSeedScore += 2 ** (winner.round - 1) * seed;
      }
    })

    this.setState({
      coryScore: cScore,
      mattScore: mScore,
      christinaScore: chScore,
      corySeedScore: cSeedScore,
      mattSeedScore: mSeedScore,
      christinaSeedScore: chSeedScore,
    });
  }

  render() {
    if (!(this.teams && this.cory && this.christina && this.matt && this.winners)) {
      return <div>Loading</div>
    }
    const scores = {
      cory: this.state.coryScore,
      corySeed: this.state.corySeedScore,
      matt: this.state.mattScore,
      mattSeed: this.state.mattSeedScore,
      ch: this.state.christinaScore,
      chSeed: this.state.christinaSeedScore,
    }
    return (
      <div>
        <Table 
          teams={this.teams}
          winners={this.winners}
          matt={this.matt}
          cory={this.cory}
          christina={this.christina}
          scores={scores}
        />
      </div>
    );
  }
}

export default App;
