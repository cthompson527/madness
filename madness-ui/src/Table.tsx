import * as React from 'react';
import { Table } from 'semantic-ui-react';
import * as _ from 'lodash';

export type Team = { name: string, seed: number };
export type Winner = { team: string, round: number };

interface TableProps {
    teams: Team[];
    winners: Winner[];
    matt: {pick: string, round: number }[];
    cory: {pick: string, round: number }[];
    christina: {pick: string, round: number }[];
    scores: { cory: number, corySeed: number, matt: number, mattSeed: number, ch: number, chSeed: number };
}

export default (props: TableProps) => {
    const getSeed = (team: string) => {
        return (_.find(props.teams, { name: team }) as Team).seed;
    };

    const correctPick = (name: string, winner: Winner) => {
        const n = name.toLowerCase();
        return _.find(props[n], { pick: winner.team, round: winner.round }) ? true : false;
    };

    const allSamePicks = (coryPick: boolean, mattPick: boolean, christinaPick: boolean) => {
        if (coryPick !== mattPick) {
            return false;
        } else if (coryPick !== christinaPick) {
            return false;
        } else if (mattPick !== christinaPick) {
            return false;
        }
        return true;
    };

    const generateTable = (winners: Winner[]) => {
        return winners.map(winner => {
            const cPos = correctPick('Cory', winner);
            const mPos = correctPick('Matt', winner);
            const chPos = correctPick('Christina', winner);
            if (allSamePicks(cPos, mPos, chPos)) {
                return <Table.Row key={`${winner.team}${winner.round}`}/>;
            }
            return (
                <Table.Row key={`${winner.team}${winner.round}`}>
                    <Table.Cell 
                        colSpan={2} 
                        positive={cPos}
                        negative={!cPos}
                    >
                        <b><i>{winner.round}</i></b> - {winner.team} <i>({getSeed(winner.team)})</i>
                    </Table.Cell>
                    <Table.Cell 
                        colSpan={2}
                        positive={mPos}
                        negative={!mPos}
                    >
                        <b><i>{winner.round}</i></b> - {winner.team} <i>({getSeed(winner.team)})</i>
                    </Table.Cell>
                    <Table.Cell
                        colSpan={2}
                        positive={chPos}
                        negative={!chPos}
                    >
                        <b><i>{winner.round}</i></b> - {winner.team} <i>({getSeed(winner.team)})</i>
                    </Table.Cell>
                </Table.Row>
            );
        });
    };
    
    return (
        <Table celled={true}>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell rowSpan={2}>Cory</Table.HeaderCell>
                    <Table.HeaderCell>Score: {props.scores.cory}</Table.HeaderCell>
                    <Table.HeaderCell rowSpan={2}>Matt</Table.HeaderCell>
                    <Table.HeaderCell>Score: {props.scores.matt}</Table.HeaderCell>
                    <Table.HeaderCell rowSpan={2}>Christina</Table.HeaderCell>
                    <Table.HeaderCell>Score: {props.scores.ch}</Table.HeaderCell>
                </Table.Row>
                <Table.Row>
                    <Table.HeaderCell>Seed Score: {props.scores.corySeed}</Table.HeaderCell>
                    <Table.HeaderCell>Seed Score: {props.scores.mattSeed}</Table.HeaderCell>
                    <Table.HeaderCell>Seed Score: {props.scores.chSeed}</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {generateTable(props.winners)}
            </Table.Body>
        </Table>
    );
};
