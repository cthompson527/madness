package main

import (
	"log"
	"net/http"
	"os"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"

	"gitlab.com/cthompson527/madness/go-mad/gomad"
)

func main() {
	env := os.Getenv("APP_ENV")
	err := gomad.SetEnvironmentVariables("env.json", env)
	checkErr(err)

	switch env {
	case "production":
		log.Println("Running api server in production mode")
	case "development":
		log.Println("Running api server in dev mode")
	case "testing":
		log.Println("Running api server in testing mode")
	default:
		log.Fatal("you must define an environment!")
		os.Exit(1)
	}

	db, err := gomad.GetDB()
	checkErr(err)

	db.AutoMigrate(&gomad.User{})
	db.AutoMigrate(&gomad.League{})
	gomad.Many2ManyFIndex(&gomad.User{}, &gomad.League{})

	s, err := gomad.GetSchema()
	checkErr(err)
	schema := graphql.MustParseSchema(s, &gomad.Query{})

	http.Handle("/query", &relay.Handler{Schema: schema})
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
