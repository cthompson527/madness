package gomad

// BoolP accepts a bool and returns a ptr to that bool
func BoolP(b bool) *bool {
	return &b
}

// StrP accepts a string and returns a ptr to that string
func StrP(s string) *string {
	return &s
}
