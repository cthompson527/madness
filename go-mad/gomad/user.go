package gomad

import (
	"context"
	"time"

	uuid "github.com/satori/go.uuid"
)

// User is the user structure holding first and last name of the user
type User struct {
	UserID    uuid.UUID `gorm:"type:uuid; primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	FirstName string
	LastName  string
	Email     string     `gorm:"type:varchar(100);unique_index"`
	Leagues   *[]*League `gorm:"many2many:user_leagues;"`
}

// UserInput is the input used in mutations of the User object
type UserInput struct {
	FirstName string
	LastName  string
	Email     string
}

// USERID returns the uuid of the user
func (u *User) USERID(ctx context.Context) string {
	return u.UserID.String()
}

// FIRSTNAME returns the firstName of the user
func (u *User) FIRSTNAME(ctx context.Context) string {
	return u.FirstName
}

// LASTNAME returns the lastName of the user
func (u *User) LASTNAME(ctx context.Context) string {
	return u.LastName
}

// EMAIL returns the email of the user
func (u *User) EMAIL(ctx context.Context) string {
	return u.Email
}

// CREATED_AT returns the time the user was created
func (u *User) CREATED_AT(ctx context.Context) string { //nolint
	return u.CreatedAt.String()
}

// UPDATED_AT returns the time the user was last updated
func (u *User) UPDATED_AT(ctx context.Context) string { //nolint
	return u.UpdatedAt.String()
}

// DELETED_AT returns the time the user was deleted
func (u *User) DELETED_AT(ctx context.Context) *string { //nolint
	deletedAt := u.DeletedAt
	if deletedAt == nil {
		return nil
	}
	return StrP(u.DeletedAt.String())
}

// LEAGUES will return an array of leagues the user is in
func (u *User) LEAGUES(ctx context.Context) *[]*League {
	var leagues []*League
	DB.Model(u).Association("Leagues").Find(&leagues)
	return &leagues
}
