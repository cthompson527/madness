package gomad

import (
	"context"
	"fmt"

	uuid "github.com/satori/go.uuid"
)

// AddUser accepts the UserInput argument and adds the new user to the database, returning true if successful
func (q *Query) AddUser(ctx context.Context, args struct{ Input UserInput }) (bool, error) {
	gid, err := uuid.NewV4()
	if err != nil {
		return false, fmt.Errorf("could not create an uuid: %v", err)
	}

	user := User{
		UserID:    gid,
		FirstName: args.Input.FirstName,
		LastName:  args.Input.LastName,
		Email:     args.Input.Email,
	}
	db := DB.Create(&user)
	if db.Error != nil {
		return false, db.Error
	}

	return true, nil
}

// AddLeague accepts the LeagueInput argument and adds a new league to the database, returning true if successful
func (q *Query) AddLeague(ctx context.Context, args struct{ Input LeagueInput }) (bool, error) {
	gid, err := uuid.NewV4()
	if err != nil {
		return false, fmt.Errorf("could not create an uuid: %v", err)
	}

	league := League{
		LeagueID: gid,
		Name:     args.Input.Name,
	}
	db := DB.Create(&league)
	if db.Error != nil {
		return false, db.Error
	}

	return true, nil
}

// AddUserToLeague adds an already defined user to a league
func (q *Query) AddUserToLeague(ctx context.Context, args struct{ Input UserToLeagueInput }) (bool, error) {
	var league League
	if err := DB.First(&league, "name=?", args.Input.LeagueName).Error; err != nil {
		return false, fmt.Errorf("could not find league %s, %v", args.Input.LeagueName, err)
	}

	var user User
	if err := DB.First(&user, "email=?", args.Input.UserEmail).Error; err != nil {
		return false, fmt.Errorf("could not find user %s, %v", args.Input.UserEmail, err)
	}

	DB.Model(&user).Association("Leagues").Append(&league)

	return true, nil
}
