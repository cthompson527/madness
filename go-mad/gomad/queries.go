package gomad

import (
	"context"
	"fmt"
)

// Query is the initial starting point of the GraphQL schema
type Query struct{}

// UserQuery is used to query for a user and return the user
type UserQuery struct {
	UserID *string
	Email  *string
}

// User returns the user with the ID sent as a parameter
func (q *Query) User(ctx context.Context, args UserQuery) (*User, error) {
	var user User
	if args.UserID != nil && args.Email != nil {
		if err := DB.First(&user, "id=? AND email=?", args.UserID, args.Email).Error; err != nil {
			return nil, fmt.Errorf("could not find user %s and %s: %v", *args.UserID, *args.Email, err)
		}
		return &user, nil
	}

	if args.UserID != nil {
		if err := DB.First(&user, "id=?", args.UserID).Error; err != nil {
			return nil, fmt.Errorf("could not find user %s: %v", *args.UserID, err)
		}
		return &user, nil
	}

	if args.Email != nil {
		if err := DB.First(&user, "email=?", args.Email).Error; err != nil {
			return nil, fmt.Errorf("could not find user %s: %v", *args.Email, err)
		}
		return &user, nil
	}

	return nil, fmt.Errorf("neither an id or email was provided to search")
}
