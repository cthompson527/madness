package gomad_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/phayes/freeport"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"gitlab.com/cthompson527/madness/go-mad/gomad"
)

func TestMain(m *testing.M) {
	err := gomad.SetEnvironmentVariables("../env.json", "testing")
	checkErr(err)

	db, err := gomad.GetDB()
	checkErr(err)

	deleteAllTables(db)

	db.AutoMigrate(&gomad.User{})
	db.AutoMigrate(&gomad.League{})
	gomad.Many2ManyFIndex(&gomad.User{}, &gomad.League{})

	s, err := gomad.GetSchemaOf("../schema.graphql")
	checkErr(err)
	schema := graphql.MustParseSchema(s, &gomad.Query{})

	port, err := freeport.GetFreePort()
	checkErr(err)
	os.Setenv("API_PORT", fmt.Sprintf("%d", port))

	server := httptest.NewServer(&relay.Handler{Schema: schema})
	os.Setenv("API_URL", server.URL)

	os.Exit(m.Run())
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

type Variables map[string]interface{}

var queries = []struct {
	Name      string
	Query     string
	Variables Variables
	Resp      string
}{
	{
		"empty response",
		removeInvalid(EmptyResponseAndFirstNameQuery),
		Variables{"email": "a@b.com"},
		`{"data":{"user":null},"errors":[{"message":"could not find user a@b.com: record not found","path":["user"]}]}`,
	},
	{
		"add user mutation",
		removeInvalid(AddUserMutationQuery),
		Variables{"email": "a@b.com", "firstName": "Cory", "lastName": "Thompson"},
		`{"data":{"addUser":true}}`,
	},
	{
		"firstName response",
		removeInvalid(EmptyResponseAndFirstNameQuery),
		Variables{"email": "a@b.com"},
		`{"data":{"user":{"firstName":"Cory"}}}`,
	},
	{
		"add league mutation",
		removeInvalid(AddLeagueMutationQuery),
		Variables{"name": "1st and 30"},
		`{"data":{"addLeague":true}}`,
	},
	{
		"add user to league mutation",
		removeInvalid(AddUserToLeagueMutationQuery),
		Variables{"userEmail": "a@b.com", "leagueName": "1st and 30"},
		`{"data":{"addUserToLeague":true}}`,
	},
}

func TestLeagues(t *testing.T) {
	for _, tt := range queries {
		client := &http.Client{}
		requestURI := fmt.Sprintf("%s/query", os.Getenv("API_URL"))
		t.Run(tt.Name, func(t *testing.T) {
			b, err := json.Marshal(tt.Variables)
			checkErr(err)
			variables := string(b)
			requestBody := strings.NewReader(`{"query":` + tt.Query + `,"variables": ` + variables + "}")
			fmt.Println(requestBody)
			resp, err := client.Post(requestURI, "application/json", requestBody)
			checkErr(err)
			defer resp.Body.Close()

			body, err := ioutil.ReadAll(resp.Body)
			checkErr(err)

			response := string(body)
			if response != tt.Resp {
				t.Errorf("expected: %s, got: %s", tt.Resp, response)
			}
		})
	}
}

func deleteAllTables(db *gorm.DB) {
	db.DropTableIfExists("user_leagues")
	db.DropTableIfExists(&gomad.User{})
	db.DropTableIfExists(&gomad.League{})
}

func removeInvalid(s string) string {
	s = strings.Replace(s, "\n", "", -1)
	return strings.Replace(s, "\t", "", -1)
}

const EmptyResponseAndFirstNameQuery = `
"query($email: String!) {
	user(email: $email) {
		firstName
	}
}"
`

const AddUserMutationQuery = `
"mutation($email: String!, $firstName: String!, $lastName: String!) {
	addUser(input: { email: $email, firstName: $firstName, lastName: $lastName })
}"
`

const AddLeagueMutationQuery = `
"mutation($name: String!) {
	addLeague(input: { name: $name })
}"
`

const AddUserToLeagueMutationQuery = `
"mutation($userEmail: String!, $leagueName: String!) {
	addUserToLeague(input: { userEmail: $userEmail, leagueName: $leagueName })
}"
`
