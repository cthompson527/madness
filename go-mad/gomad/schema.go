package gomad

import "io/ioutil"

// GetSchemaOf returns, as a string, the schema read from the file path
func GetSchemaOf(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

// GetSchema returns, as a string, the default schema read at schema.graphql
func GetSchema() (string, error) {
	return GetSchemaOf("schema.graphql")
}
